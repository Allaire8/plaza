//@prepros-prepend ../../node_modules/jquery/dist/jquery.min.js
//@prepros-prepend ../../node_modules/bootstrap/dist/js/bootstrap.min.js
//@prepros-prepend ../../node_modules/owl.carousel/dist/owl.carousel.min.js
//@prepros-prepend ../../node_modules/lazysizes/lazysizes.min.js
//@prepros-prepend ../../node_modules/sweetalert2/dist/sweetalert2.js
//@prepros-prepend ../../node_modules/lightgallery/dist/js/lightgallery-all.min.js
//@prepros-prepend ../../node_modules/lightgallery/modules/lg-fullscreen.min.js
//@prepros-prepend ../../node_modules/lightgallery/modules/lg-thumbnail.min.js
//@prepros-prepend ../../node_modules/lightgallery/modules/lg-pager.min.js

$(document).ready(function(){
  $(".owl-carousel").owlCarousel();

  $("#lightgallery").lightGallery({
     selector: '.item',
     thumbnail:true,
     mode: 'lg-fade',
  });
});


$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})



$(document).on('click', '#success', function(e) {
  swal(
    'Success',
    'done',
    'success'
  )
});
