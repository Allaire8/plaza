<!doctype html>
<?php include_once('header.php')  ?>
<html class="no-js" lang="fr">

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Plaza owl carousel</title>
	<meta name="description" content="Description du contenu de la page">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- BOOTSTRAP -->

	<link rel="stylesheet" href="css/style.css">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

</head>

<body>
	<div class="ref">
	<h3>A ROUEN HOTEL DE VILLE</h3>
	<h3>A LOUER APPARTEMENT 4 PIECES 78M²</h3>
	<h4>Loyer 950€/mois charges comprises</h4>
	</div>



	<div class="owl-carousel owl-theme" id="lightgallery">
		<?php for($i = 1; $i<=6; $i++): ?>
    <div><a class="item" href="img/<?php echo $i;?>.png"><img src="img/<?php echo $i;?>.png" alt""></div></a>
	<?php endfor; ?>


  	</div>

		


  	<div class="icons">
  		<i class="fas fa-hand-holding-usd fa-3x"></i>
  		<i class="fas fa-building fa-3x"></i>
  		<i class="fas fa-square fa-3x"></i>
  		<p>950 €/mois</p>
  		<p>Appartement</p>
  		<p>78M²</p>

  	</div>

<?php include_once('footer.php')  ?>
